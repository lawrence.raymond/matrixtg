import telebot
import paramiko

# Замените этот токен на ваш токен Telegram бота
TOKEN = ''

# Имя пользователя и данные SSH-ключа
SSH_USERNAME = ''
SSH_HOST = ''
SSH_PORT = 25431
SSH_KEY_PATH = 'id_rsa'  # Замените на путь к вашему SSH-ключу

# Создаем объект бота
bot = telebot.TeleBot(TOKEN)

# Функция для установки SSH-соединения
def establish_ssh_connection():
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    private_key = paramiko.RSAKey(filename=SSH_KEY_PATH)
    ssh.connect(SSH_HOST, SSH_PORT, SSH_USERNAME, pkey=private_key)
    return ssh


# Команда /list
@bot.message_handler(commands=['list'])
def get_users_list(message):
    try:
        ssh = establish_ssh_connection()
        stdin, stdout, stderr = ssh.exec_command('/etc/userslist.sh')
        output = stdout.read().decode()
        ssh.close()

        bot.reply_to(message, f"Список пользователей:\n{output}")
    except Exception as e:
        bot.reply_to(message, f"Ошибка: {str(e)}")


# Команда /gethash
@bot.message_handler(commands=['gethash'])
def get_user_hash(message):
    try:
        username = message.text.split(' ')[1]
        ssh = establish_ssh_connection()
        cmd = f'/etc/getpasswordhash.sh \'{username}\''
        stdin, stdout, stderr = ssh.exec_command(cmd)
        output = stdout.read().decode()
        ssh.close()

        bot.reply_to(message, f"Хэш для пользователя {username}:\n{output}")
    except Exception as e:
        bot.reply_to(message, f"Ошибка: {str(e)}")


# Команда /changehash
@bot.message_handler(commands=['changehash'])
def change_user_hash(message):
    try:
        command_parts = message.text.split(' ')
        username = command_parts[1]
        new_hash = command_parts[2]
        ssh = establish_ssh_connection()
        cmd = f'/etc/edituserhash.sh \'{username}\' $\'{new_hash}\''
        stdin, stdout, stderr = ssh.exec_command(cmd)
        output = stdout.read().decode()
        ssh.close()

        bot.reply_to(message, f"Измененный хэш для пользователя {username}:\n{output}")
    except Exception as e:
        bot.reply_to(message, f"Ошибка: {str(e)}")


# Запускаем бота
bot.polling()
